package p1;

import java.util.ArrayList;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Demo1 {
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
		Hello ob;
		ob = (Hello)context.getBean("hello");
		ob.display();
		Holiday ho;
		Holiday ho2;
		Holiday ho3;
		ho = (Holiday)context.getBean("holiday");
		ho2 = (Holiday)context.getBean("holiday2");
		ho3 = (Holiday)context.getBean("holiday3");
		ArrayList<Holiday> arr = new ArrayList<Holiday>();
		arr.add(ho);
		arr.add(ho2);
		arr.add(ho3);
		ListOfHolidays list = new ListOfHolidays();
		list.setArr(arr);
		for(Holiday h: arr) {
			System.out.println(h.getName()+" "+h.getDate());
		}
		Employee emp;
		emp = (Employee)context.getBean("emp1");
		System.out.println(emp);
		Employee emp1;
		emp1 = (Employee)context.getBean("emp2");
		System.out.println(emp1);
		((ClassPathXmlApplicationContext)context).close();
	}
}
