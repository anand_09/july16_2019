package p1;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Demo2 {
	public static void main(String[] args) {
		ApplicationContext context = new AnnotationConfigApplicationContext(JavaContainer.class);
		Employee emp;
		emp = context.getBean(Employee.class);
		System.out.println(emp);
		ListOfHolidays list;
		list = context.getBean(ListOfHolidays.class);
		System.out.println(list);
		((ClassPathXmlApplicationContext)context).close();
	}
}
