package p1;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class JavaContainer {
	
	@Bean
	@Scope("singleton")
	public Hello get1() {
		return new Hello();
	}
	
	@Bean
	public Employee get2() {
		Employee ob;
		ob = new Employee("Shubham", 23);
		return ob;
	}
	
	@Bean
	public ListOfHolidays get3() {
		ListOfHolidays list;
		list = new ListOfHolidays();
		list.getArr().add(new Holiday("15 Aug", "Independence Day"));
		list.getArr().add(new Holiday("26 Jan", "Republic Day"));
		return list;
	}
	
}
