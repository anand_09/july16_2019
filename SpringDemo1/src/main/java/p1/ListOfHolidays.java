package p1;

import java.util.ArrayList;

public class ListOfHolidays {
	private ArrayList<Holiday> arr ;

	public ArrayList<Holiday> getArr() {
		return arr;
	}

	public void setArr(ArrayList<Holiday> arr) {
		this.arr = arr;
	}

	@Override
	public String toString() {
		return "ListOfHolidays [arr=" + arr + "]";
	}

	public ListOfHolidays(ArrayList<Holiday> arr) {
		super();
		this.arr = arr;
	}


	public ListOfHolidays() {
		arr = new ArrayList<Holiday>();
	}
	

}
