package p2;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;

@Aspect
@Order(0)
public class ArithmeticAspect {
	
	@Before("execution( * *.*(double,double))")
	public void check1(JoinPoint jpoint) {
		for(Object x : jpoint.getArgs()) {
			double v = (Double)x;
			if(v<0) {
				throw new IllegalArgumentException("Number cannot be nagative");
			}
		}
	}
	
	@AfterReturning(pointcut = "execution( * *.*(double,double))", returning = "retvalue")
	public void check2(JoinPoint jpoint, Object retvalue) {
		double v = (Double)retvalue;
		if(v<0) {
			throw new IllegalArgumentException("Answer cannot be less than zero");
		}
	}
	
	@AfterReturning(pointcut = "execution( * *.*(double,double))", returning = "retvalue")
	public void check3(JoinPoint jpoint, Object retvalue) {
		double v = (Double)retvalue;
		if(v<0 || v>200) {
			throw new IllegalArgumentException("Answer must be between zero and two hundred");
		}
	}

}
