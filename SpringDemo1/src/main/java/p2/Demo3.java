package p2;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Demo3 {

	public static void main(String[] args) {
		try {
			Arithmetic ob;
			ApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
			ob = (Arithmetic)context.getBean("arithmetical");
			System.out.println(ob.add(5, 6.0));
			System.out.println(ob.sub(15.5, 2));
			System.out.println(ob.mul(50, 6.0));
			System.out.println(ob.div(15, 18));
			((ClassPathXmlApplicationContext)context).close();
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

}
